import os
if os.path.exists("index.html"):
  os.remove("index.html")

file = open("projects.txt","r")
data = file.readlines()
file.close()
html = open("index.html","w")
header = """
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Plain HTML site using GitLab Pages</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href="https://pages.gitlab.io/plain-html/">Plain HTML Example</a>
      <a href="https://gitlab.com/pages/plain-html/">Repository</a>
      <a href="https://gitlab.com/pages/">Other Examples</a>
    </div>

    <h1>Hello World!</h1>

    <p>
      This is a simple plain-HTML website on GitLab Pages, without any fancy static site generator.
    </p>
    <ul>
"""
footer = """
  </ul>
  </body>
</html>
"""
html.write(header)
print(data)
for i in data:
    print(i)
    html.write("<li>"+i+"</li>")
html.write(footer)
html.close()